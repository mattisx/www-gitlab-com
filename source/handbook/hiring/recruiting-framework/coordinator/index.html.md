---
layout: markdown_page
title: "Recruiting Process - Coordinator Tasks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Process Framework - Coordinator Tasks
{: #framework-coord}

### Step 3/C: Post vacancy, check details

Follow the steps outlined in the handbook to [complete and open a vacancy in Greenhouse](https://about.gitlab.com/handbook/hiring/vacancies/#recruiting-team-tasks). Ensure that the role is assigned to the correct coordinator and recruiter, that the hiring team and interview plan are correct and appropriate access is given, that the correct notifications are set up, and that the job description is populated and has a LinkedIn job wrapping code. Publish the vacancy on the GitLab jobs page and add it to the team page.

### Step 11/C: Schedule first interview

Once a candidate has provided their availability in Greenhouse, the coordinator will utilize the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) to determine scheduling needs, in this case, scheduling a first round interview the hiring manager. If the repo is outdated or you are unsure of the interview process, reach out to the recruiter.

### Step 14/C: Schedule team interviews

Once a candidate has provided their availability in Greenhouse for the second-round interviews, the coordinator will utilize the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) and schedule all second-round interviews with the interview team. These interviews will be scheduled at the same time, to take place either on the same day (ideal) or over the course of a 2-3 days. The coordinator should be sure to inform the candidate that each subsequent interview is contingent on the success of the prior interview.

### Step 20/C: Initiate background check

Once notified by the recruiter, the coordinator will [initiate a background check](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#initiating-a-background-check) for the candidate. The coordinator will continue to monitor the background check until finalized. Once finalized, the coordinator will notify the recruiter that it is completed. If the background check has red flags, the coordinator will loop in the People Ops Generalist and Senior Director of Legal Affairs for future action. Some flags, such as incorrect start/end dates at previous companies can be investigated by the coordinator; if consistencies are still found, then the check can be escalated. Driving-related offenses are not considered flags at GitLab and can be ignored.

### Step 23/C: Send contract

Once the verbal offer and offer email are sent out, the coordinator will send the contract to the applicant, using DocuSign in Greenhouse. On rare occasion, the coordinator may have to create the contract outside of Greenhouse using Google Docs; if this is the case, the coordinator needs to have a People Business Partner review the contract for accuracy before sending it out for signature.

