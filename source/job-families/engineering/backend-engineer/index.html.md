---
layout: job_family_page
title: "Backend Engineers"
---

# Backend Engineering Roles at GitLab

Backend Engineers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and frontend developers to solve common goals.

Unless otherwise specified, all Backend Engineering roles at GitLab share the following
requirements and responsibilities:

<a id="intermediate-requirements"></a>
#### Requirements
* [Significant professional experience with Ruby and Rails](#ruby-experience)
* Professional experience with any other technologies that may be required by
  the [specialty](#specialties)
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
* Comfort working in a highly agile, intensely iterative software development process
* Demonstrated ability to onboard and integrate with an organization long-term
* Positive and solution-oriented mindset
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* Self-motivated and self-managing, with strong organizational skills.
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization

#### Nice-to-haves

* Experience in a peak performance organization, preferably a tech startup
* Experience with the GitLab product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Computer science education or equivalent experience
* Passionate about/experienced with open source and developer tools

#### Responsibilities
* Develop features and improvements to the GitLab product in a secure,
  well-tested, and performant way
* Collaborate with Product Management and other stakeholders within Engineering (Frontend, UX, etc.) to maintain a high bar for quality in a fast-paced, iterative environment
* Advocate for improvements to product quality, security, and performance
* Solve technical problems of moderate scope and complexity. 
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Recognize impediments to our efficiency as a team ("technical debt"), propose and implement solutions
* Represent GitLab and its values in public communication around specific projects and community contributions. 
* Confidently ship small features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

***

<a id="ruby-experience"></a>

Unless otherwise specified below, please note that a significant amount of
experience with Ruby is a **strict** requirement.

We would love to hire all great backend engineers, regardless of the language
they have most experience with, but at this point we are looking for
engineers
who can get up and running within the GitLab code base very quickly and without
requiring much training, which limits us to engineers with a large amount of
existing experience with Ruby, and preferably Rails too.

For a time, we also considered candidates with little or no Ruby and Rails
experience for this position, because we realize that programming skills are to
a large extent transferable between programming languages, but we are not
currently doing that anymore for the reasons described in the [merge
request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695) that
removed the section from this listing that described that policy.

***

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

### Junior Backend Engineer

Junior Backend Engineers share the same requirements and responsibilities outlined above, but typically join with less or alternate experience than a typical Backend Engineers.

### Senior Backend Engineer

The Senior Backend Engineer role extends the [Backend Engineer](#intermediate-requirements) role.

#### Responsibilities

* Advocate for improvements to product quality, security, and performance that have particular impact across your team.
* Solve technical problems of high scope and complexity. 
* Exert influence on the overall objectives and long-range goals of your team.
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions. 
* Provide mentorship for Junior and Intermediate Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship moderately sized features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

***

A Senior Backend Engineer may want to pursue the [engineering management track](/job-families/engineering/backend-engineer/#engineering-manager) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

**Note:** Staff and above positions at GitLab are more of a role than just a "level". We prefer to bring people in as Senior and let the team elevate them to Staff due to an outstanding work history within GitLab.

***

### Staff Backend Engineer

The Staff Backend Engineer role extends the [Senior Backend Engineer](#senior-backend-engineer) role.

#### Responsibilities

* Advocate for improvements to product quality, security, and performance that have particular impact across your team and others.
* Solve technical problems of the highest scope and complexity for your team.
* Exert significant influence on the overall objectives and long-range goals of your team.
* Shepherd the definition and improvement of our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Drive innovation on the team with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively seek out difficult impediments to our efficiency as a team ("technical debt"), propose and implement solutions that will enable the entire team to iterate faster
* Represent GitLab and its values in public communication around broad initiatives, specific projects, and community contributions. Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your team. 
* Provide mentorship for all Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship large features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

### Distinguished Backend Engineer

The Distinguished Backend Engineer role extends the [Staff Backend Engineer](#staff-backend-engineer) role.

* At this level the person's contribution plays to their strength and role on the team. These contributions come in different forms such as: Ship _large_ feature sets with team, completes _feature discovery_ independently, publishes technical blogs and speaks at conferences, interfaces with customers and provides technical direction to stakeholders (Product, Sales, others)
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for other backend engineers
* Author architecture documents for epics
* Hold team members accountable within their roles

### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Backend Engineer](#distinguished-backend-engineer) role.

* Advocate for improvements to product quality, security, and performance that impact all of Engineering at GitLab.
* Solve technical problems of the highest scope and complexity for the entire
  organization.
* Exert significant influence on the overall objectives and long-range goals of GitLab.
* Ensure that our standards for style, maintainability, and best practices are suitable for the unique problems of scale and diversity of use represented by the GitLab product. Maintain and advocate for these standards through code review.
* Drive innovation across Engineering with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively seek out and prioritize our toughest technical challenges with a goal of creating significant improvement for GitLab's use, ease of development, and/or technical efficiency.
* Represent GitLab and its values in public communication in all aspects of our software development lifecycle and public relations. Interact with customers and other external stakeholders as a consultant and spokesperson for critical projects and aspects of our technical architecture.
* Provide mentorship for Senior and Staff Engineers at the company to help them grow in their technical responsibilities and to share your great expertise across the organization.
* Confidently ship immense or otherwise extremely high-impact features and improvements with minimal guidance and support from other members of the organization.
* Help create the sense of psychological safety in the department

## Engineering Management Roles at GitLab

Managers in the engineering department at GitLab see the team as their product. While they are technically credible and know the details of what backend engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Engineering Manager

Unless otherwise specified below, all Engineering Manager roles at GitLab share
the following requirements and responsibilities:

#### Responsibilities

* Help your engineers grow their skills and experience
* Author project plans for epics
* Run agile project management processes
* Conduct code reviews, and make technical contributions to product
  architecture as well as getting involved in solving bugs and delivering small
  features
* Actively seek and hire globally-distributed talent
* Conduct managerial interviews for candidates, and train the team to screen candidates
* Contribute to the sense of psychological safety on your team
* Generate and implement process improvements
* Hold regular 1:1's with all members their team
* Foster technical decision making on the team, but make final decisions when necessary
* Author project plans for epics
* Draft quarterly OKRs
* Train engineers to screen candidates and conduct managerial interviews
* Improve product quality, security, and performance

#### Requirements

* Exquisite communication: Regularly achieve consensus amongst departments
* 5 years or more experience in a leadership role with current technical experience
* In-depth experience with Ruby on Rails, Go, and/or Git, in addition to any
  experience required by the position's [specialty](#specialties)
* Excellent written and verbal communication skills
* You share our [values](/handbook/values), and work in accordance with those values

#### Nice-to-haves

* Experience in a peak performance organization
* Deep Ruby on Rails experience
* Product company experience
* Startup experience
* Enterprise software company experience
* Computer science education or equivalent experience
* Passionate about open source and developer tools

#### Hiring Process

Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 60 minute first interview with a Director of Engineering, Backend
* Next, candidates will be invited to schedule a 45 minute second peer interview with an Engineering Manager
* Next, candidates will be invited to schedule a 45 minute third interview with one or more members of the Engineering team
* Next, candidates will be invited to schedule a 45 minute fourth interview with our VP of Engineering
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Director of Engineering

The Director of Engineering role extends the [Engineering Manager](#engineering-manager) role.

* Hire a world class team of managers and engineers to work on their teams
* Help their managers and engineers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your _teams_
* _Drive_ technical and process improvements
* _Drive_ quarterly OKRs
* _Drive_ agile project management process
* _Own_ product quality, security, and performance
* Represent the company publicly at conferences

The Director of Engineering, Backend at GitLab manages multiple backend teams in the product development organization. They see their teams as their product. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both engineers and managers alike. But they can also grow the existing talent on their teams. Directors are leaders that model the behaviors we want to see in our teams and hold others accountable when necessary. And they create the collaborative and productive environment in which engineers and engineering managers do their work.

#### Responsibilities

* Hire and manage multiple backend teams that lives our [values](/handbook/values/)
* Measure and improve the happiness and productivity of the team
* Manage the agile development process
* Drive quarterly OKRs
* Work across sub-departments within engineering
* Own the quality, security and performance of the product
* Write public blog posts and speak at conferences

#### Requirements

* 10 years managing multiple software engineering teams
* Experience in a peak performance organization
* Deep Ruby on Rails experience
* Product company experience
* Startup experience
* Enterprise software company experience
* Computer science education or equivalent experience
* Passionate about open source and developer tools
* Exquisite communication skills

#### Nice-to-haves

* Kubernetes, Docker, Go, Helm, or Linux administration
* Online community participation
* Remote work experience
* Significant open source contributions

### VP of Engineering

The VP of Engineering role extends the [Director of Engineering](#director-of-engineering) role.

* Drive recruiting of a world class team
* Help their directors, managers, and engineers grow their skills and experience
* Measure and improve the happiness of engineering
* Make sure the handbook is used and maintained in a transparent way
* _Sponsor_ technical and process improvements
* _Own_ the sense of psychological safety of the department
* _Set_ quarterly OKRs around company goals
* _Define_ the agile project management process
* Spend time with customers to understand their needs and issues
* _Be accountable for_ product quality, security, and performance

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Distribution

The Distribution team closely partners with the rest of the engineering organization to build, configure, and automate GitLab installation.
GitLab's distribution team is tasked with creating a seamless installation experience for customers and community users across multitude of platforms.

Distribution engineering is interlaced with the broader development team in supporting newly created features.
Notably, the infrastructure team is the distribution team's biggest internal customer, so there is significant team interdependency.
The Distribution team also provides significant variety in tasks and access to a diversity of projects, including helping out on various community packaging projects.
This is reflected in the job requirements: we are tasked with creating and maintaining a Cloud Native GitLab deployment and upgrade methods, Omnibus GitLab package installation across multiple Linux based Operating Systems, and various Cloud providers deployment methods (such as AWS Cloudformation, GC Deployment Manager and so on).

#### Requirements
* Experience with Docker and Kubernetes in production use cases
* Chef experience (writing complex cookbooks from scratch, custom providers, custom resources, etc.)
* Extensive Linux experience, comfortable between Debian and RHEL based systems
* Basic knowledge of packaging archives such as .deb and .rpm package archives

### Packaging

Packaging engineers are focused on creating the binary repository management
system that will extend our Continuous Integration (CI) functionality to allow access
and management of artifacts manipulated by projects.

By extending the current CI artifacts system, the Packaging team will expose GitLab as
a package repository allowing access to the most common package managers, e.g.
Maven and APT and similar. Additionally, the Packaging team is improving
the Container Registry and is responsible for items listed under [Packaging product category](/handbook/product/categories/).

#### Requirements
* Experience with Go
* Experience with working and reasoning with applications running at scale
* Experience or strong interest in developing as a DevOps engineer
* Experience or strong interest in software packaging and package distribution systems

#### Responsibilities
* Develop the architecture by extending existing features
* Work with the Distribution team on replacing their current delivery system
* Create and maintain observability of the newly defined features
* Work with customers on defining their needs to replace existing package repository solutions

### Secure

Focus on security features for GitLab. This role will specifically focus on security; if you want to work with Ruby on Rails and not security, please apply to our Backend Engineer role instead. This role will report to and collaborate directly with the Secure Engineering Manager.

#### Requirements
* Strong Go and/or Ruby engineer with security expertise or proven security interest.
* Passion and interest toward security (scanning, dependencies, etc.).
* Experience in using GitLab and GitLab CI.

#### Responsibilities
* Develop security tools from proposal to polished end result.
* Integrating 3rd party security tools into GitLab.
* Complete our internal Advisories Database.
* Manage metadata related to dependencies.
* Key aspects of this role are focused on security tools and features.
* The complexity of this role will increase over time.
* If you are willing to stick to working on these features for at least a year, then this role is for you.

### Configuration

  The configuration team works on GitLab's Application Control Panel, Infrastructure Configuration features, our ChatOps product, Feature flags, and our entire Auto DevOps feature set. It is part of our collection of Ops Backend teams.

#### Requirements
* Experienced engineer who is capable of leading and growing a team of senior engineers.
* For this position, a significant amount of experience with Ruby is a strict requirement. Experience with Go is a plus as we expect that you will likely work on Go during your journey at GitLab.
* Experience with Docker, Kubernetes platform development.
* Experience or interest in functions-as-a-service.

#### Responsibilities
* Implement and improve upon our constellation of configuration feature set.
* Work with the PM team to execute on the roadmap.
* Ensure we deliver on our commitments to the market by communicating clearly with stakeholders.
* Implement the appropriate monitoring and alerting on new and existing features owned by the team.
* Help others adopt and use the configuration features.

### CI/CD

CI/CD Backend Engineers are primarily tasked with improving the Continuous Integration (CI)
and Continuous Deployment (CD) functionality in GitLab. Engineers should be willing to learn Kubernetes and Container Technology. CI/CD Engineers should always have three goals in mind:
1. Provide value to the user and communicate such with product managers,
2. Introduce features that work at scale and in untrusting environments,
3. Always focus on defining and shipping [the Minimal Viable Change](/handbook/product/#the-minimally-viable-change).

We, as a team, cover end-to-end integration of CI/CD in GitLab, with components being written in Rails and Go.
We work on a scale of processing a few million of CI/CD jobs on GitLab.com monthly.
CI/CD engineering is interlaced with a number of teams across GitLab.
We build new features by following our [direction](/direction/#ci--cd).
Currently, we focus on providing a deep integration of Kubernetes with GitLab:
1. by automating application testing and deployment through Auto DevOps,
1. by managing GitLab Runners on top of Kubernetes,
1. by working with other teams that provide facilities to monitor all running applications,
1. in the future implement A-B testing, feature flags, etc.

Additionally, we also focus on improving the efficiency, performance, and scalability of all aspects of CI/CD:
1. Improve performance of developer workflows, e.g. faster CI testing, by improving parallelization,
1. Improve performance of implementation, ex.:by allowing us to run 10-100x more in one year,
1. Identify and add features needed by us, ex.:to allow us to test more reliable and ship faster.

The CI/CD Engineering Manager also does weekly stand-up with a team and product managers to talk about plan for the work in the upcoming week and coordinates a deployment of CI/CD related services with infrastructure team.

#### Requirements
* Go developer with a lot of Kubernetes production experience is a plus

### Geo

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html)
is an enterprise product feature that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well.

#### Requirements
* Deep experience architecting and implementing fault-tolerant, distributed systems
* Experience building and scaling highly-available systems
* In-depth experience with Ruby on Rails, Go, and/or Git

#### Requirements (Staff Level)
* Architect Geo and Disaster Recovery products for GitLab
* Identify ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
* Instrument and monitor the health of distributed GitLab instances
* Educate all team members on best practices relating to high availability

### Growth

Growth Engineers work with a cross-functional team to influence the growth of
GitLab as a business. In helping us iterate and learn rapidly, these
engineers
enable us to more effectively meet the needs of potential users.

#### Requirements
* Strong self-direction (this team is being bootstrapped)
* Experience with A/B, multivariate, or other data-driven methods of testing
* Comfort multitasking in a highly iterative environment

### Quality

Quality Engineers are primarily tasked with improving the productivity of
the GitLab developers (from both GitLab Inc and the rest of the community), and
making the GitLab project maintainable in the long-term.

See the description of the [Quality team](/handbook/engineering/quality) for more details.
The position also involves working with the community as
[merge request coach](/job-families/merge-request-coach), and working together with our
[Backend Engineers](/job-families/engineering/backend-engineer) to respond and
address issues from the community.

### Gitaly

Gitaly is a new service in our architecture that handles git and other filesystem operations for GitLab instances, and aims to improve reliability and performance while scaling to meet the needs of installations with thousands of concurrent users, including our site GitLab.com. This position reports to the Gitaly Lead.

#### Responsibilities
* Participate in architectural discussions and decisions surrounding Gitaly.
* Scope, estimate and describe tasks to reach the team’s goals.
* Collaborate on designing RPC interfaces for the Gitaly service
* Instrument, monitor and profile Gitaly in the production environment.
* Build dashboards and alerts to monitor the health of your services.
* Conduct acceptance testing of the features you’ve built.
* Educate all team members on best practices relating to high availability.

#### Requirements:
* Mandatory: production experience building, debugging, optimising software in large-scale, high-volume environments.
* Mandatory: Solid production Ruby experience.
* Highly desirable: Experience working with Go. It’s important that candidates must be willing to learn and work in both Go and Ruby.
* Highly desirable: experience with gRPC.
* Highly desirable: a good understanding of git’s internal data structures or experience running git servers. You can reason about software, algorithms, and performance from a high level.
* Understanding of how to build instrumented, observable software systems.
* Experience highly-available systems in production environments.

### Meltano (BizOps Product)

[Meltano](https://gitlab.com/meltano/meltano) is an early stage project at GitLab focused on delivering an open source framework for analytics, business intelligence, and data science. It leverages version control, data science tools, CI, CD, Kubernetes, and review apps.

A Meltano Engineer will be tasked with executing on the vision of the Meltano project, to bring the product to market.

#### Requirements
* A passion for data science and analytics
* Experience with doing initial prototyping, architecture, and engineering work
* In-depth experience with Python (no Ruby or Rails experience required for this
  role)
* Experience with Kubernetes, Helm, and CI/CD is a **strict requirement**

### Database

A database specialist is an engineer that focuses on database related changes
and improvements. You will spend the majority of your time making application
changes to improve database performance, availability, and reliability; though
you will also spend time working on the database infrastructure that powers
GitLab.com.

Unlike the [Database Engineer](/job-families/engineering/database-engineer/) position the database
specialist title focuses more on application development and less on knowledge
of PostgreSQL. As such Ruby knowledge is absolutely required, but the
requirements for PostgreSQL knowledge / experience are less strict compared to
the Database Engineer position.

#### Example Projects

* Rewriting the database queries and related application logic used for retrieving [subgroups](https://docs.gitlab.com/ee/user/group/subgroups/index.html#subgroups)
* Rewriting code used for importing projects from other platforms (e.g. [GitHub](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14731))
* Adding trend analysis to monitoring to better detect performance and availability changes on GitLab.com
* Analyzing tables and optimizing them by adding indexes, breaking them up into separate tables, or by removing unnecessary columns.
* Reviewing database related changes submitted by other engineers
* Documenting database best practices or patterns to avoid

#### Requirements

* At least 2 years of experience running PostgreSQL in production environments
* At least 5 years of experience working with Ruby
* At least 3 years of experience with Ruby on Rails or other Ruby frameworks
  such as Sinatra or Hanami
* Solid understanding of SQL
* Significant experience working in a distributed production environment

### Release Management

Release management specialist is an engineer that focuses on improving the engineering workflows, creates new tools, improves release process
and works closely with the whole Engineering team to ensure that
every GitLab release reaches the public in time.

Release Management specialist also leads a [Crew](/company/team/structure/index.html#crew) that gets organized per monthly release cycle and carry out the release tasks.

#### Responsibilities

* Identifies issues in architecture of each component of GitLab and proposes
solutions
* Works with individual teams on defining and implementing solutions
* Enforces frameworks that allow engineers to write code that scales with demand
* Helps teams instrument their code and helps recognize parts of code that could benefit from increased observability
* Prioritize architectural issues that have caused a negative impact in the past
* Works closely with Infrastructure teams to control the impact of application code running in user facing products
* Improve the tools used to create a GitLab release and deploy to GitLab.com
* Enforce the [GitLab Release Process](https://gitlab.com/gitlab-org/release/docs)
* Select [Release Managers](/handbook/engineering/index.html#release-managers) and [Release Trainees](/handbook/engineering/index.html#release-managers) monthly
* Organize Release Managers and Trainees tasks and creates release schedules
* Helps communicate the release schedule clearly with others
* Develop monitoring and alerting to measure release process velocity
* Identify process bottlenecks and introduce optimizations

### Gitter

[Gitter](https://gitter.im) specialists are full-stack JavaScript developers who are able to write JavaScript code that is shared between multiple environments. Gitter uses uses a JavaScript stack running Node.js on the server, and bundled with webpack on the client. The [iOS](https://gitlab.com/gitlab-org/gitter/gitter-ios-app), [Android](https://gitlab.com/gitlab-org/gitter/gitter-android-app), MacOS (Cocoa) and [Linux/Windows (NW.js)](https://gitlab.com/gitlab-org/gitter/desktop/) clients reuse [much of the same codebase](https://gitlab.com/gitlab-org/gitter/webapp) but also require some knowledge of Objective-C, Swift and Java. Gitter uses MongoDB, Redis, and Elasticsearch for backend storage.

#### Requirements

* Strong client-side JavaScript experience
* Strong production Node.js experience
* Highly desirable: MongoDB, Elasticsearch, and Redis experience
* Desirable: Some Java, Objective-C or Swift experience building mobile apps
* Desirable: DevOps experience, working with Linux, Ansible, AWS or similar products

#### Responsibilities

* Fix prioritized issues from the issue tracker
* Triage issues (duplicates, clarification, reproduction steps, prioritization)
* Create high quality frontend and backend code
* Review community contributions
* Provide second-level support to the Production Team to ensure that all Gitter production services remain stable
* Document tribal knowledge, particularly around runbooks and production incident processes
* Keep an eye on Sentry to find regressions and ensure application errors are addressed
* Continually improve the quality of Gitter by using discretion of where you think changes are needed
* Continue to migrate the codebase from old repository locations to GitLab, while open-sourcing as much of it as possible
* Maintain the iOS, Android, and desktop applications
* Provide community support for Gitter via [Gitter rooms](https://gitter.im/gitterHQ/gitter), [Twitter](https://twitter.com/gitchat), Zendesk, etc
